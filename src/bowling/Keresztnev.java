package bowling;

import allapotter.Operator;

public class Keresztnev extends Operator {

    static final int FERENC = 1, GEZA = 2, IMRE = 3, JOZSEF = 4, KALMAN = 5;
    int v;
    int keresztnev;

    public Keresztnev(int v, int keresztnev) {
        this.v = v;
        this.keresztnev = keresztnev;
    }
}
