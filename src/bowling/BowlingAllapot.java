package bowling;

import allapotter.Allapot;
import allapotter.HibasOperatorException;
import allapotter.Operator;

import java.util.Arrays;
import java.util.HashSet;

public class BowlingAllapot extends Allapot {

    static {
        operatorok = new HashSet<>();
        int[] atlag = {170, 185, 195, 200, 210};
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 5; j++) {
                operatorok.add(new Keresztnev(i, j));
                operatorok.add(new Golyoszin(i, j));
                operatorok.add(new Atlag(i, atlag[j - 1]));
            }
        }
    }

    private int[][] h;

    public BowlingAllapot() {
        h = new int[6][4];
    }

    private BowlingAllapot(BowlingAllapot szulo) {
        h = new int[6][4];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 4; j++) {
                h[i][j] = szulo.h[i][j];
            }
        }
    }

    @Override
    public boolean celAllapot() {
        return h[5][3] != 0;
    }

    @Override
    public boolean elofeltetel(Operator op) throws HibasOperatorException {
        if (op instanceof Keresztnev) {
            Keresztnev k = (Keresztnev) op;
            int v = k.v;
            int keresztnev = k.keresztnev;
            if (h[v][1] != 0) {
                return false;
            }
            if (v != 1 && h[v - 1][1] == 0) {
                return false;
            }
            for (int i = 1; i <= 5; i++) {
                if (i < v && h[i][1] == keresztnev) {
                    return false;
                }
            }
            if (v == 4 && keresztnev != Keresztnev.IMRE) {
                return false;
            }
            if (keresztnev == Keresztnev.IMRE && v != 4) {
                return false;
            }
            if (v == 1 && keresztnev == Keresztnev.FERENC) {
                return false;
            }
            if (v == 3 && keresztnev == Keresztnev.GEZA) {
                return false;
            }
            return true;
        } else if (op instanceof Golyoszin) {
            Golyoszin g = (Golyoszin) op;
            int v = g.v;
            int szin = g.szin;
            if (h[v][2] != 0) {
                return false;
            }
            if (v == 1 && h[5][1] == 0) {
                return false;
            }
            if (v != 1 && h[v - 1][2] == 0) {
                return false;
            }
            for (int i = 1; i <= 5; i++) {
                if (i < v && h[i][2] == szin) {
                    return false;
                }
            }
            if (v == 5 && szin != Golyoszin.VOROS) {
                return false;
            }
            if (szin == Golyoszin.VOROS && v != 5) {
                return false;
            }
            if (szin == Golyoszin.KEK && h[v][1] == Keresztnev.JOZSEF) {
                return false;
            }
            if (v == 4 && szin != Golyoszin.FEKETE) {
                return false;
            }
            if (szin == Golyoszin.FEKETE && v != 4) {
                return false;
            }
            if (szin == Golyoszin.KEK && v == 2) {
                return false;
            }
            if (h[v][1] == Keresztnev.JOZSEF && szin == Golyoszin.LILA) {
                return false;
            }
            return true;
        } else if (op instanceof Atlag) {
            Atlag a = (Atlag) op;
            int v = a.v;
            int atlag = a.atlag;
            if (h[v][3] != 0) {
                return false;
            }
            if (v == 1 && h[5][2] == 0) {
                return false;
            }
            if (v != 1 && h[v - 1][3] == 0) {
                return false;
            }
            for (int i = 1; i <= 5; i++) {
                if (i < v && h[i][3] == atlag) {
                    return false;
                }
            }
            if (h[v][2] == Golyoszin.KEK && !(atlag < h[1][3])) {
                return false;
            }
            for (int i = 1; i <= 5; i++) {
                if (h[v][2] == Golyoszin.KEK && h[i][1] == Keresztnev.JOZSEF && !(atlag > h[i][3])) {
                    return false;
                }
            }
            for (int i = 1; i <= 5; i++) {
                if (h[v][1] == Keresztnev.JOZSEF && h[i][2] == Golyoszin.KEK && h[i][3] != 0 && !(atlag < h[i][3])) {
                    return false;
                }
            }
            for (int i = 1; i <= 5; i++) {
                if (v == 5 && h[i][1] == Keresztnev.JOZSEF && atlag != h[i][3] + 10){
                    return false;
                }
            }
            if (h[v][1] == Keresztnev.KALMAN && atlag != 195){
                return false;
            }
            if (atlag == 195 && h[v][1] != Keresztnev.KALMAN){
                return false;
            }
            if (atlag == 210 && h[v][2] == Golyoszin.ZOLD){
                return false;
            }
            return true;
        }
        throw new HibasOperatorException();
    }

    @Override
    public Allapot alkalmaz(Operator op) throws HibasOperatorException {
        if (op instanceof Keresztnev) {
            Keresztnev k = (Keresztnev) op;
            BowlingAllapot uj = new BowlingAllapot(this);
            uj.h[k.v][1] = k.keresztnev;
            return uj;
        } else if (op instanceof Golyoszin) {
            Golyoszin g = (Golyoszin) op;
            BowlingAllapot uj = new BowlingAllapot(this);
            uj.h[g.v][2] = g.szin;
            return uj;
        } else if (op instanceof Atlag) {
            Atlag a = (Atlag) op;
            BowlingAllapot uj = new BowlingAllapot(this);
            uj.h[a.v][3] = a.atlag;
            return uj;
        }
        throw new HibasOperatorException();
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= 5; i++) {
            switch (i){
                case 1:
                    sb.append("Aradi: ");
                    break;
                case 2:
                    sb.append("Budai: ");
                    break;
                case 3:
                    sb.append("Cecei: ");
                    break;
                case 4:
                    sb.append("Devai: ");
                    break;
                case 5:
                    sb.append("Egri: ");
                    break;
            }
            sb.append(" ");
            switch (h[i][1]){
                case Keresztnev.FERENC:
                    sb.append("Ferenc ");
                    break;
                case Keresztnev.GEZA:
                    sb.append("Geza ");
                    break;
                case Keresztnev.IMRE:
                    sb.append("Imre ");
                    break;
                case Keresztnev.JOZSEF:
                    sb.append("Jozsef ");
                    break;
                case Keresztnev.KALMAN:
                    sb.append("Kalman ");
                    break;
                default:
                    sb.append("0");
                    break;
            }
            sb.append(" ");
            switch (h[i][2]){
                case Golyoszin.FEKETE:
                    sb.append("fekete ");
                    break;
                case Golyoszin.KEK:
                    sb.append("kek ");
                    break;

                case Golyoszin.LILA:
                    sb.append("lila ");
                    break;

                case Golyoszin.VOROS:
                    sb.append("voros ");
                    break;

                case Golyoszin.ZOLD:
                    sb.append("zold ");
                    break;
                default:
                    sb.append("0");
                    break;
            }
            sb.append(" ");
            sb.append(h[i][3]);
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }
}
