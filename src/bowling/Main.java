package bowling;

import kereso.Csucs;
import kereso.Kereso;
import kereso.keresografos.szisztematikus.SzelessegiKereso;

public class Main {
    public static void main(String[] args) {
        Kereso kereso = new SzelessegiKereso(new BowlingAllapot(), Kereso.MEGOLDAS_ALLAPOT | Kereso.OSSZES_MEGOLDAS);
        kereso.keres();
        System.out.println(kereso);
        for (Csucs csucs : kereso.getTerminalisok()) {
            System.out.println("\nEgy megoldas:\n");
            kereso.kiirMegoldas(csucs);
        }
        System.out.println("Megoldasok szama: " + kereso.getTerminalisok().size());
    }
}
